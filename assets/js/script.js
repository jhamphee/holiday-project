function menuIcon(x) {
    let nav_menu = document.getElementById("nav-icon");
    let menu = document.getElementsByClassName("mobile-nav");
    x.classList.toggle("change");
    if (menu[0].style.display === "block") {
        menu[0].style.display = "none";
    } else {
        menu[0].style.display = "block";
    }
}

function dropdown(){
    let sub = document.querySelector(".sub-menu");

    if (sub.style.display === "block") {
        sub.style.display = "none";
    } else {
        sub.style.display = "block";
    }
}
